module.exports = function() {
  $('.navbar-collapse').on('click', 'a.close', function () {
    $('.navbar-collapse').collapse('hide');
  });

  $('.navbar-collapse').on('click', 'a.menu', function () {
    $('.navbar-collapse').collapse('hide');
  });

  $('a.learn-more').on('click', function(e) {
    e.preventDefault();
    let id = $(this).data('id');
    $('#' + id).addClass('open');
  });

  $('.product-info .close, .product-info .bg').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.product-info').removeClass('open');
  });
};
console.log('app.js has loaded!');

;(function($) {
  "use strict";

  // modules requires
  let customFunctions = require('./customFunctions');
  let customScrollbar = require('./customScrollbar');
  let clickFunctions = require('./clickFunctions');

  // functions
  function parallax(e, target, layer) {
    let layer_coeff = 10 / layer;
    let x = -(e.pageX / 50);
    let y = -(e.pageY / 200);
    $(target).css({
      transform: 'translate3d(' + x + 'px, ' + y + 'px, 0)'
    });
  }
  $('.product-info .wrapper .info').sticky({
    silent: true,
    observeChanges: true
  });

  $('.product-info .wrapper .info').mCustomScrollbar({
    autoHideScrollbar: true
  });

  let refreshCubesBoolean = true;
  function refreshCubeDots() {
    setInterval(function() {
      $('.merchant-tools').html($('.merchant-tools').html());
    }, 10000);
    refreshCubesBoolean = false;
  }

  $('.arrow-up').click(function() {
    $('html, body').animate({
      scrollTop : 0
    }, 500);
    return false;
  });

  // document ready functions
  $(document).ready(function() {

    // modules
    customFunctions();

    // functions
    $('.same-height').matchHeight();

  }); // end of document ready

  // window load functions
  $(window).on('load', function() {

    // modules
    customScrollbar();
    let sliderValue;

    let currenciesBottom = $('#currency-slider-bottom').owlCarousel({
      loop: false,
      nav: false,
      dots: false,
      items: 6,
      responsive: {
        0: {
          items: 1
        },
        768: {
          items: 4
        },
        992: {
          items: 6
        }
      }
    });
    $('#currency-slider-top').owlCarousel({
      loop: false,
      nav: false,
      dots: false,
      navText: ["<img src='./images/icons/chevron-left.svg'>", "<img src='./images/icons/chevron-right.svg'>"],
      items: 6,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        768: {
          items: 4,
          nav: true
        },
        992: {
          items: 6
        }
      },
      onInitialized: function() {
        sliderValue = 0;
      },
      onChange: function(event) {
        if ( sliderValue < event.property.value ) {
          sliderValue = event.property.value;
          currenciesBottom.trigger('next.owl.carousel');
        } else {
          sliderValue = event.property.value;
          currenciesBottom.trigger('prev.owl.carousel');
        }
        // $('.customNextBtn').click(function() {
        //   owl.trigger('next.owl.carousel');
        // });
        // $('.customPrevBtn').click(function() {
        //   owl.trigger('prev.owl.carousel');
        // });
      }
    });

    $(".fingerprint-svg .fill path").each(function() {
      let length = $(this)[0].getTotalLength();
      $(this).attr("stroke-dasharray", length);
      $(this).attr("stroke-dashoffset", length);
    });

  }); // end of window load

  // initialization
  clickFunctions();
  $('.selectpicker').selectpicker();


  $('#becomePayyex').validator({
    feedback: {
      success: 'valid',
      error: 'invalid'
    }
  });

  $('#userPhone').inputmask({
    mask: '+9 999 999 999',
    placeholder: '+1 321 123 456'
  });

  // window resize functions
  $(window).resize(function() {

  });  // end of window resize

  $('.corporate-client').mousemove(function (e) {
    parallax(e, document.getElementById('lines-parallax'), 2);
    parallax(e, document.getElementById('circle-parallax'), 2);
  });

  $('#becomePayyex').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {

    } else {
      e.preventDefault();
      $(this).addClass('rocket-send');
    }
  });

  $('#becomePayyex .form-control').on('keyup', function() {
    if ( $(this).closest('.form-group').hasClass('has-success') ) {
      $('#becomePayyex').addClass('input-valid-' + $(this).closest('.form-group').data('input'));
    }
  });

  $(document).on('scroll', function() {
    if( $(this).scrollTop() >= $('.exchange').position().top ) {
      $('.exchange').addClass('animate');
    }
    if ( $(this).scrollTop() >= $('.merchant-tools').position().top ) {
      $('.merchant-tools').addClass('animate');
      if ( refreshCubesBoolean ) {
        refreshCubeDots();
      }
    }
    if ( $(this).scrollTop() >= $('.oklink-network').position().top ) {
      $('.oklink-network').addClass('animate');
    }
    if ( $(this).scrollTop() >= $('.developers-api').position().top ) {
      $('.developers-api').addClass('animate');
    }
    if ( $(this).scrollTop() >= $('.corporate-client').position().top - 50 ) {
      console.log('add animate');
      if ( !$('.corporate-client').hasClass('fingerprint-checked-1') ) {
        $('.corporate-client').addClass('animate');
        setTimeout(function() {
          $(".fingerprint-svg .fill path").each(function() {
            $(this).stop().animate({
              'stroke-dashoffset': 0
            }, {
              duration: 1000,
              easing: "linear"
            });

          });
          setTimeout(function() {
            $('.corporate-client').addClass('fingerprint-checked-1');
          }, 1500);
        }, 1000);
      }
    }
  });

})(jQuery);
